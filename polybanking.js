fetch = require('node-fetch')
crypto = require('crypto')

function polybanking(config_id, keyREQ, keyIPN, keyAPI, base_url = "https://polybanking.agepoly.ch") {
	this.config_id = config_id;
	this.keyREQ = keyREQ;
	this.keyIPN = keyIPN;
	this.keyAPI = keyAPI;
	this.base_url = base_url;

    this.new_transaction = function (ref, amount, extra_data="", twint_only=0) {
    	let url = this.base_url+"/paiements/start/"
    	let init_data = {
    		config_id: this.config_id,
    		reference: ref,
    		amount: amount,
    		extra_data: extra_data,
    	}
    
    	post_data = sortObject(init_data);
     	post_data.sign = hash(post_data, this.keyREQ);
    
    	var queryString = Object.keys(post_data).map(key => key + '=' + post_data[key]).join('&');

    	console.log(queryString);
    
    	return fetch(url, {
            method: 'POST',
            body: queryString,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then(res => {return res.json();})	
        .then(res => {
        	if(res.url != '') {
        		return res.url;
        	} else {
        		return Promise.reject("Erreur : "+res.status);
        	}
        });
    }

    this.check_ipn = function (body) {
	    post_data = sortObject(body);
	    delete post_data.sign;
 	    if(body.sign != hash(post_data, this.keyIPN) || body.config != this.config_id){
 	    	return false;
 	    }
 	    return true;
	}
}

function escape_chars(s) {
     return s.toString().replace(';', '!!').replace('=', '??');
}

function sortObject(in_data){
	var out_data = {};
	Object.keys(in_data).sort().forEach(function(key) {
  		out_data[key] = in_data[key];
	});
	return out_data;
}

function hash(obj, secret) {
	let str = "";
	for ( const [key, value] of Object.entries(obj)) {
        str = str + escape_chars(key) + '=' + escape_chars(value) + ';' + secret + ';';
	}

	return crypto.createHash('sha512').update(str).digest('hex');
}

module.exports = polybanking;