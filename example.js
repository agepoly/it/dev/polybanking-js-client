polybanking = require('./polybanking.js');

let secretReq = "483ad5ac4a5fce5b2284109e061474913347f90811e673f50f7ffbf27857bc6bd7f5f8bf322a2014a42176b0485063adc7544e1fcdf78ef9cf13eb3c5523beda";
let secretIpn = "36ab98a2e1332e8a95a4b07356c45a7c1dc91f9efc07321fbf2abd465edc5ffa7a1e4c4cb5b7c502d98b3aaf5d9545cd60dcf379d304d7adb49dc6e7108f1a38";
let config_id = 73;

(async () => {
	pb = new polybanking(config_id, secretReq, secretIpn, "");
	console.log(await pb.new_transaction("1"+Date.now(), 1000, "testteo"+Date.now()));

	body = {
    	postfinance_status: "9",
    	postfinance_status_good: "True",
    	reference: "11606725531119",
    	sign: "3a588ad33111e1a80f3076d03804315e8385474b3ecf96ed64ba7d6cec55f8ef2888e3560190044d190a415ea7508a470de1a67161a8d4619ee5aec4d4e3b4db",
    	last_update: "2020-11-30 08:39:45+00:00",
    	config: "73"
	}
	console.log(pb.check_ipn(body));

	body = {
    	postfinance_status: "11", //changed a parameterto test a malicous user
    	postfinance_status_good: "True",
    	reference: "11606725531119",
    	sign: "3a588ad33111e1a80f3076d03804315e8385474b3ecf96ed64ba7d6cec55f8ef2888e3560190044d190a415ea7508a470de1a67161a8d4619ee5aec4d4e3b4db",
    	last_update: "2020-11-30 08:39:45+00:00",
    	config: "73"
	}
	console.log(pb.check_ipn(body));
})()
